package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"sync"
	"sync/atomic"
	"time"
)

const workersCount = 5
const chanQueueLength = 20

func handleA(w http.ResponseWriter, r *http.Request) {
	time.Sleep(1 * time.Second)
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Hello World A"))
	return
}

// ограничение на "воркеров". ограничивает количество одновременных работающих
// проблема данного метода - статичное количество воркеов
// Плюсы - просто
func workersChannelMiddleware(next http.HandlerFunc) http.HandlerFunc {
	reqCount := atomic.Int64{}
	wChan := make(chan struct{}, workersCount)
	queue := atomic.Int64{}
	return func(w http.ResponseWriter, r *http.Request) {
		// Очередь
		queue.Add(1)
		// Занять место
		wChan <- struct{}{}
		queue.Add(-1)
		reqCount.Add(1)
		fmt.Println("Request:", reqCount.Load(), "Queue:", queue.Load())
		// Выполнить запрос
		next(w, r)
		// Отдать место
		<-wChan
	}
}

func main() {
	w := NewWorkersHandler(workersCount)

	r := mux.NewRouter()
	r.HandleFunc("/A", workersChannelMiddleware(handleA))
	r.HandleFunc("/B", w.Middleware(handleA))

	http.ListenAndServe(":8000", r)
}

// Второй подход. Можно контроллировать количество воркеров
// Минус - для паники нужен свой рекавер на каждого воркера
type Task struct {
	f   http.HandlerFunc
	w   http.ResponseWriter
	r   *http.Request
	res chan struct{}
}

type WorkersHandler struct {
	closeChans []chan struct{}
	taskChan   chan Task
	mx         *sync.Mutex
}

func NewWorkersHandler(workersCount int) *WorkersHandler {
	w := &WorkersHandler{
		closeChans: make([]chan struct{}, 0, workersCount),
		taskChan:   make(chan Task, chanQueueLength),
		mx:         &sync.Mutex{},
	}
	w.Add(workersCount)
	return w
}

func (wh *WorkersHandler) Add(workersCount int) {
	wh.mx.Lock()
	defer wh.mx.Unlock()
	for i := 0; i < workersCount; i++ {
		closeChan := make(chan struct{})
		wh.closeChans = append(wh.closeChans, closeChan)

		go wh.worker(closeChan)
	}
}

func (wh *WorkersHandler) Remove(workersCount int) {
	wh.mx.Lock()
	defer wh.mx.Unlock()
	if len(wh.closeChans) < workersCount {
		workersCount = len(wh.closeChans)
	}

	for _, c := range wh.closeChans[len(wh.closeChans)-workersCount:] {
		close(c)
	}
	wh.closeChans = wh.closeChans[:len(wh.closeChans)-workersCount]
}

func (wh *WorkersHandler) worker(close <-chan struct{}) {
	for {
		select {
		case <-close:
			return
		case task, ok := <-wh.taskChan:
			if !ok {
				return
			}
			if task.f == nil {
				// можно поорать - ошибка
				task.res <- struct{}{}
				continue
			}

			task.f(task.w, task.r)
			task.res <- struct{}{}
		}
	}
}

func (wh *WorkersHandler) Middleware(next http.HandlerFunc) http.HandlerFunc {
	reqCount := atomic.Int64{}
	queue := atomic.Int64{}

	return func(w http.ResponseWriter, r *http.Request) {
		queue.Add(1)
		resChan := make(chan struct{})
		wh.taskChan <- Task{f: next, w: w, r: r, res: resChan}
		<-resChan
		queue.Add(-1)
		reqCount.Add(1)
		fmt.Println("Done request:", reqCount.Load(), "Queue:", queue.Load())
	}
}
