package tests

import (
	"math/rand"
	"sync"
	"testing"
)

var (
	m  = map[string]string{}
	mx = &sync.Mutex{}

	sm = sync.Map{}
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) string {
	rnd := rand.New(rand.NewSource(0))
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rnd.Intn(len(letterRunes))]
	}
	return string(b)
}

func BenchmarkMapMux(b *testing.B) {
	for i := 0; i < b.N; i++ {
		mx.Lock()
		key := RandStringRunes(10)
		m[key] = "here"
		if m[key] != "here" {
			b.Error(m[key], "!= here")
		}
		mx.Unlock()
	}
}

func BenchmarkMapSync(b *testing.B) {
	for i := 0; i < b.N; i++ {
		key := RandStringRunes(10)
		sm.Store(key, "here")
		val, ok := sm.Load(key)
		if !ok {
			b.Error(key, "not found")
			continue
		}
		valStr, ok := val.(string)
		if !ok {
			b.Error(key, "not string")
			continue
		}
		if valStr != "here" {
			b.Error(valStr, "!= here")
		}
	}
}
